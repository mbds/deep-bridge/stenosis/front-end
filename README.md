# Medical Prediction

Merci d'utiliser notre site de prédiction de complications médicales.

Cette plateforce a été conçu pour permettre l'analyse semi-automatisé des données médicales fournies pour pouvoir prédire des cas médicaux ; dans un premier temps, nous avons implémenté les AVC et les endofuites, mais il est possible d'en rajouter d'autres.
Cela permettra à l'avenir d'effectuer une suite d'étapes de Machine Learning, et le tout paramétrable.

Voici un guide rapide de nos pages et de leurs contenus : 


## 1. Connexion / Enregistrement

Ces pages permettent de s'identifier en tant que personnel qualifié pour utiliser le site ; un mail suffit pour se connecter.

A l'avenir, nous pouvons envisager des alertes/notifications pour ceux qui y sont intéressés (par exemple pour signaler une mise à jour de la base, une nouvelle entrée, ...).


## 2. Nouveau cas d'étude

Cette page est réservée aux Administrateurs ; elle permet d'implémenter de nouveaux cas à prédire ; pour cela, l'utilisateur doit rentrer tous les paramètres demandés ; tous ne sont pas obligatoires.

Une fois ce cas crée, il sera visible dans la page des formulaires.


## 3. Résultats du patient

Ce tableau permet de visualiser tous les patients et leurs risques de développer, par exemple, un AVC.

Pour l'instant, tout est réuni dans un seul tableau, mais une prochaine mise à jour permettra de séparer les 2 prédictions (Sténose et Endofuites) dans 2 tableaux différents.


## 4. Formulaire cas patient 

Nous pouvons ajouter dans notre base de nouveaux cas via cette interface ; nous choisissons d'abord quelle est la complication à prédire, puis nous importons le fichier afin de le soumettre à nos algorithmes (en cours) et ainsi obtenir les probabilités de son développement.
