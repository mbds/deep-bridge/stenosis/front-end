(function () {
    'use strict';

    angular
        .module('app')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['UserService', '$rootScope','$scope','$http','fileUploadService'];
    function HomeController(UserService, $rootScope,$scope,$http,fileUploadService) {

        var vm = this;
        $scope.serverResponse="";
        UserService.GetByUsername($rootScope.globals.currentUser.username)
            .then(function (user) {
                vm.user = user;
            });

        /*
        Faire appel à l'api pour récuperer les cas d'études
         */
        $http.get('http://localhost:44225/cases')
            .then(function successCallback(response) {
                console.log(response.data);
                $scope.cases=response.data;

            }, function errorCallback(response) {

                console.log("error in getting the data");

            });


        $scope.uploadFile = function () {
            var file = $scope.myFile;
            var uploadUrl = "http://localhost:44225/caas", //Url of webservice/api/server
                promise = fileUploadService.uploadFileToUrl(file, uploadUrl);

            promise.then(function (response) {
                $scope.serverResponse = response;
            }, function () {
                $scope.serverResponse = 'An error has occurred';
            })
        };
    }

})();
