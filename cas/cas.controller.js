(function () {
    'use strict';

    angular
        .module('app')
        .controller('CasController', CasController);

    CasController.$inject = ['UserService', '$rootScope', '$scope', 'casService'];

    function CasController(UserService, $rootScope, $scope, casService) {
        var vm = this;
        $scope.serverResponse = "";
        $scope.tasks=['segmentation','classification'];
        var libelle=$scope.libelle;
        var description=$scope.description;
        var selectedTask=$scope.selectedTask;
        var data=$scope.data;
        var imResize=$scope.imResize;
        var aug=$scope.aug;
        var normalize=$scope.normalizee;
        var segMethode=$scope.segMethode;
        var classifMethod=$scope.classifMethod;
        var batch=$scope.batch;
        var epoch=$scope.epoch;
        var rate=$scope.rate;
        var optimizeur=$scope.optimizeur;


        $scope.uploadFile = function () {
            var file = $scope.myFile;
            var uploadUrl = "http://localhost:44225/caas", //Url of webservice/api/server
                promise = casService.uploadToUrl(file,libelle,description,selectedTask,data,imResize,aug,normalize,segMethode,classifMethod,batch,epoch,rate,optimizeur,uploadUrl);

            promise.then(function (response) {
                $scope.serverResponse = response;
            }, function () {
                $scope.serverResponse = 'An error has occurred';
            })
        };


    }

})();
