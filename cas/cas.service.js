(function () {
    'use strict';

    angular.module('app').service('casService',['$http','$q', function ($http, $q) {


        this.uploadToUrl = function (file,libelle,description,selectedTask,data,imResize,aug,normalize,segMethode,classifMethod,batch,epoch,rate,optimizeur,uploadUrl) {
            //FormData, object of key/value pair for form fields and values
            var fileFormData = new FormData();
            fileFormData.append('file', file);
            fileFormData.append('libelle', libelle);
            fileFormData.append('description', description);
            fileFormData.append('selectedTask', selectedTask);
            fileFormData.append('data', data);
            fileFormData.append('imResize', imResize);
            fileFormData.append('aug', aug);
            fileFormData.append('normalize', normalize);
            fileFormData.append('segMethode', segMethode);
            fileFormData.append('classifMethod', classifMethod);
            fileFormData.append('batch', batch);
            fileFormData.append('epoch', epoch);
            fileFormData.append('rate', rate);
            fileFormData.append('optimizeur', optimizeur);
            console.log(fileFormData);
            console.log(fileFormData.get('libelle'));
            var deffered = $q.defer();

            $http.post(uploadUrl, fileFormData, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}

            }).then(function successCallback(response) {

                deffered.resolve(response);

            }, function errorCallback(response) {

                deffered.reject(response);

            });

            return deffered.promise;
        }
    }]);
})();
