(function () {
    'use strict';

    angular
        .module('app')
        .controller('resultController', resultController);

    resultController.$inject = ['UserService', '$rootScope', '$scope'];

    function resultController(UserService, $rootScope, $scope) {

        /*
        Récupération de résultat d'après le serveur backend
         */
        $http.get('http://localhost:44225/result')
            .then(function successCallback(response) {
                console.log(response.data);
                $scope.cases = response.data;

            }, function errorCallback(response) {

                console.log("error in getting the data");

            });
    }


})();
